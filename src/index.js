import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './App';
import Login from './login';
import Signin from './signin';
import Cities from './cities';
import * as serviceWorker from './serviceWorker';

const routing = (
    <Router>          
        <Route exact path="/" component={App} />
        <Route path="/login" component={Login} />
        <Route path="/signin" component={Signin} />  
        <Route path="/cities" component={Cities} />         
    </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

serviceWorker.unregister();
