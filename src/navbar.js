import React, {useState} from 'react';
import login from './images/login.png';
import './navbar.css';
import {
    Navbar,
    Nav,
    NavItem,
    NavLink,
    NavbarBrand,
    NavbarToggler,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Collapse   } from 'reactstrap';    

    const NavBar = (props) => {

        const [isOpen, setIsOpen] = useState(false);
        const toggle = () => setIsOpen(!isOpen);

        return(
            
            <Navbar color="light" light expand="md">  
                <NavbarBrand>
                    <UncontrolledDropdown>
                        <DropdownToggle>
                            <img src={login} className="login" alt="Login" />
                        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem href='#'>
                                Create Account
                            </DropdownItem>
                            <DropdownItem href='#'>
                                Log In
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </NavbarBrand> 
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>                                   
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink href="#">Opcion 1</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#">Opcion 2</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#">Opcion 3</NavLink>
                        </NavItem>
                    </Nav> 
                </Collapse>               
            </Navbar>
            
        )}


export default NavBar