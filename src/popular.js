import React , { useState } from 'react'
import './popular.css';
import sitio1 from './images/sitio1.jpg';
import sitio2 from './images/sitio2.jpg';
import sitio3 from './images/sitio3.jpg';
import sitio4 from './images/sitio4.jpg';
import sitio5 from './images/sitio5.jpg';
import sitio6 from './images/sitio6.jpg';
import sitio7 from './images/sitio7.jpg';
import sitio8 from './images/sitio8.jpg';
import sitio9 from './images/sitio9.jpg';
import sitio10 from './images/sitio10.jpg';
import sitio11 from './images/sitio11.jpg';
import sitio12 from './images/sitio12.jpg';

import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    Row, Col, Container
  } from 'reactstrap';

  const items = [
    {
      src1: sitio1,
      src2: sitio2,
      src3: sitio3,
      src4: sitio4
    },
    {
      src1: sitio5,
      src2: sitio6,
      src3: sitio7,
      src4: sitio8
    },
    {
      src1: sitio9,
      src2: sitio10,
      src3: sitio11,
      src4: sitio12
    }
  ];

 

  const Popular = (props) => {
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);
  
    const next = () => {
      if (animating) return;
      const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
      setActiveIndex(nextIndex);
    }
  
    const previous = () => {
      if (animating) return;
      const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
      setActiveIndex(nextIndex);
    }
  
    const goToIndex = (newIndex) => {
      if (animating) return;
      setActiveIndex(newIndex);
    }
  
    const slides = items.map((item) => {
      return (
        <CarouselItem 
          onExiting={() => setAnimating(true)}
          onExited={() => setAnimating(false)}
          key={item.src1}
        >
            <Container>
               <Row noGutters>                   
                    <img className='imagenesCar' src={item.src1} />                  
                    <img className='imagenesCar' src={item.src2} />                  
               </Row>
               <Row noGutters>                
                    <img className='imagenesCar' src={item.src3} />                  
                    <img className='imagenesCar' src={item.src4} />                
               </Row>
            </Container>
                    
        </CarouselItem>
      );
    });
  
    return (
      <Carousel className='imgCarousel'
        activeIndex={activeIndex}
        next={next}
        previous={previous}
      >
        <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
        {slides}
        <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
      </Carousel>
    );
  }   

export default Popular